package org.louie.tests;

import org.junit.Test;
import org.louie.Order;
import org.louie.Transaction;

import static org.junit.Assert.*;

public class TransactionTest {

    @Test
    public void shouldProcessAnAONTransaction() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 19.01, 1234, 4323);
        Transaction transaction = new Transaction(order1, order2);
        transaction.trade();

        assertEquals("Sell order units should equal buyerTotalUnits", transaction.getBuyerTotalUnits(), transaction.getSell().getUnits());
        assertEquals("Buy order units should equal the requested amount", transaction.getBuyerTotalUnits(), transaction.getBuy().getUnits());
        assertEquals("Buy order units should equal 100", 100, transaction.getBuyerTotalUnits());
        assertEquals("Sell order units left should be 100", 0, transaction.getSellerUnitsLeft());
        assertTrue("Buy order total price should equal total purchase price",
                transaction.getSellerTotalPrice() == transaction.getBuy().getPrice() * transaction.getBuy().getUnits());
        assertEquals("Fill type should be COMPLETE", transaction.getFillType(), Transaction.FillType.COMPLETE);
    }

    @Test
    public void shouldProcessAPartialOrder() {

    }

    @Test
    public void shouldNotProcessNonMatchingOrders() {
    }
}
