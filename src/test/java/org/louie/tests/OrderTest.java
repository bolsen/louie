package org.louie.tests;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;

import org.louie.Order;
import org.louie.OrderQualifications;
import org.louie.Transaction;
import org.louie.TransactionFactory;


public class OrderTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void shouldNotMatchWhenOrderTypesAreTheSame() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4323);

        assertFalse("The orders should not have the same order type", OrderQualifications.areOrderTypesDifferent(order1, order2));
    }


    @Test
    public void shouldNotMatchWhenPricesAreNotBenefitingBothParties() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 19.01, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 20.00, 1234, 4323);
        assertFalse("The order should have the same price.", OrderQualifications.canPurchaseOccur(order1, order2));
    }

    @Test
    public void shouldMatchWhenPricesAreEqual() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 20.00, 1234, 4323);
        assertTrue("The order prices should match", OrderQualifications.canPurchaseOccur(order1, order2));
    }

    @Test
    public void shouldMatchWhenSalePriceIsLowerThanBuyPrice() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 19.00, 1234, 4323);
        assertTrue("The orders should match when the sell is cheaper", OrderQualifications.canPurchaseOccur(order1,order2));
    }

    @Test
    public void shouldNotMatchWhenSecurityIdIsNotTheSame() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 20.00, 1235, 4323);
        assertFalse("The order should have the same security id", OrderQualifications.areSecurityIdsSame(order1, order2));

    }

    @Test
    public void shouldNotMatchWhenUserIdIsTheSame() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 20.00, 1234, 4321);
        assertFalse("The order should not have the same user ids", OrderQualifications.areUserIdsDifferent(order1, order2));

    }

    @Test
    public void shouldNotMatchWhenSellOrderHasLessUnitsThanBuyOrder() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 10, 20.00, 1234, 4322);
        assertFalse("The sell order needs to have more or equal units to the buy order", OrderQualifications.enoughUnits(order1, order2));
    }

    @Test
    public void shouldMatchWhenSellAndBuyUnitsAreEqual() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 100, 20.00, 1234, 4322);
        assertTrue("The buy and sell order units should match", OrderQualifications.enoughUnits(order1, order2));
    }

    @Test
    public void shouldMatch() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, false, 200, 20.00, 1234, 4322);
        assertTrue("Orders should match", order1.matches(order2));
    }

    @Test
    public void testFillingOrder() {
        Order order1 = new Order(Order.OrderType.BUY, true, 100, 20.00, 1234, 4321);
        Order order2 = new Order(Order.OrderType.SELL, true, 200, 20.00, 1234, 4322);

        Transaction tx = TransactionFactory.completeTransaction(order1, order2);
    }
}
