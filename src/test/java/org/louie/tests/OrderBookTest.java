package org.louie.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.louie.Order;
import org.louie.OrderBook;
import org.louie.OrderBookException;

import static org.junit.Assert.assertEquals;

public class OrderBookTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void shouldAddANewOrder() throws OrderBookException {
        OrderBook book = new OrderBook(1234);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, 1234, 4321);

        book.add(order);
        assertEquals("Should have added to the buy order map", 1, book.getBuyOrders().size());

        Order order2 = new Order(Order.OrderType.SELL, true, 100, 0.01, 1234, 4321);

        book.add(order2);
        assertEquals("Should have added to the sell order map", 1, book.getSellOrders().size());

    }

    @Test
    public void shouldNotAddAnOrderIfTheSecurityDoesNotMatch() throws OrderBookException {
        thrown.expect(OrderBookException.class);

        OrderBook book = new OrderBook(1234);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, 12345, 4321);

        book.add(order);

        assertEquals("Should have added to the buy order map", 0, book.getBuyOrders().size());
    }

    @Test
    public void shouldCancelAnOrder() throws Exception {

    }

    @Test
    public void testExecute() throws Exception {

    }

    @Test
    public void testSnapshot() throws Exception {

    }
}
