package org.louie.tests;

import org.junit.Test;
import org.louie.*;

import static junit.framework.Assert.*;

public class LimitTest {

    @Test
    public void shouldAddAMatchingOrder() {
        Limit limit = new Limit(0.01, 1234, Order.OrderType.BUY);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, 1234, 4321);

        limit.addOrder(order);

        assertNotNull("Order must have the limit", order.getLimit());
        assertEquals("Should have added the order", 1, limit.size());
    }

    @Test
    public void shouldNotMatchGivenDifferentOrderTypes() {
        Limit limit = new Limit(0.01, 1234, Order.OrderType.SELL);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, 1234, 4321);
        limit.addOrder(order);

        assertEquals("Should have not added the order", 0, limit.size());
    }

    @Test
    public void shouldNotMatchGivenDifferentSecurityIds() {
        Limit limit = new Limit(0.01, 1234, Order.OrderType.BUY);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, 12345, 4321);
        limit.addOrder(order);

        assertEquals("Should have not added the order", 0, limit.size());
    }

    @Test
    public void shouldNotMatchGivenDifferentPrices() {
        Limit limit = new Limit(0.01, 1234, Order.OrderType.BUY);
        Order order = new Order(Order.OrderType.BUY, true, 100, 0.03, 1234, 4321);
        limit.addOrder(order);

        assertEquals("Should have not added the order", 0, limit.size());
    }

    @Test
    public void shouldMatchAndRemove() throws LimitException, OrderBookException {
        int randSecId = (int)Math.random();
        OrderBook book = new OrderBook(randSecId);

        Order order = new Order(Order.OrderType.BUY, true, 100, 0.01, randSecId, 4321);
        book.add(order);

        Limit limit = book.getLimit(Order.OrderType.BUY, 0.01);
        assertEquals("Should have added the order", 1, limit.size());


        // We need to build an order that will match to the buy order listed. The change is the user.
        Order orderSell = new Order(Order.OrderType.SELL, true, 100, 0.01, randSecId, 4322);
        book.execute(orderSell);
        // assertNotNull("Buy order should be found", orderBuy);

        assertNull(book.getLimit(Order.OrderType.BUY, 0.01));

    }
}
