package org.louie;

import java.util.UUID;

import static java.util.UUID.*;

public class Order {
    private final UUID uuid;
    private final boolean allOrNothing;
    private int units;
    private OrderType orderType;
    private double price;
    private int securityId;
    private int userId;
    private Limit limit;

    public Order(OrderType orderType, boolean allOrNothing, int units, double price, int securityId, int userId) {
        this.orderType  = orderType;
        this.allOrNothing = allOrNothing;
        this.units      = units;
        this.price      = price;
        this.securityId = securityId;
        this.userId     = userId;
        uuid = randomUUID();
    }

    /*
     * Returns true if the order matches the provided order.
     *
     * @param order The order you are matching
     *
     * @returns boolean
     */
    public boolean matches(Order order) {

        return OrderQualifications.areOrderTypesDifferent(this, order) && OrderQualifications.canPurchaseOccur(this, order) &&
                OrderQualifications.areSecurityIdsSame(this, order) && OrderQualifications.enoughUnits(this, order) &&
               OrderQualifications.areUserIdsDifferent(this, order);
    }

    /*
     *
     */

    public void removeSelfFromLimit() throws OrderException, LimitException {
        if(limit == null) {
            throw new OrderException(this, "Could not remove self from order.");
        }

        this.getLimit().removeOrder(this);
    }

    public OrderType getOrderType() {
        return this.orderType;
    }


    public double getPrice() {
        return price;
    }

    public int getSecurityId() {
        return securityId;
    }

    public int getUserId() {
        return userId;
    }

    public int getUnits() {
        return units;
    }

    public boolean isAllOrNothing() {
        return allOrNothing;
    }

    public Limit getLimit() {
        return limit;
    }

    public void setLimit(Limit limit) {
        this.limit = limit;
    }

    public UUID getUuid() {
        return uuid;
    }

    public static enum OrderType {
        BUY, SELL
    }

}
