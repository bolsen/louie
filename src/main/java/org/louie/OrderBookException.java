package org.louie;

public class OrderBookException extends Exception {
    private final OrderBook orderBook;
    private Order order = null;

    public OrderBookException(OrderBook orderBook, Order order, String s) {
        super(s);
        this.orderBook = orderBook;
        this.order     = order;
    }

    public OrderBookException(OrderBook orderBook, String s) {
        super(s);
        this.orderBook = orderBook;
    }

    public OrderBook getOrderBook() {
        return orderBook;
    }

    public Order getOrder() {
        return order;
    }
}
