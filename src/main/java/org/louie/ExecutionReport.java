package org.louie;

import java.util.ArrayList;

/*
 * Provides a set of data points after a normal execution for a given order book.
 */
public class ExecutionReport {

    private Limit limit = null;
    private Order order = null;
    private ArrayList<Order> matchedOrders = new ArrayList<Order>();


    public ExecutionReport() {

    }

    public boolean hasLimit() {
        return limit != null;
    }

    public void setLimit(Limit limit) {
        this.limit = limit;
    }

    public boolean hasOrder() {
        return order != null;
    }

    public boolean hasMatchedOrders() {
        return matchedOrders.size() > 0;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void addMatchedOrder(Order order) {
        matchedOrders.add(order);
    }

    public void trade() {
    }
}
