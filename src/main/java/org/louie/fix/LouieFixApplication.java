package org.louie.fix;

import quickfix.*;

public class LouieFixApplication implements Application{

    private final SessionSettings settings;
    private final MessageCracker messageCracker;

    public LouieFixApplication(SessionSettings settings) {
        this.settings = settings;
        this.messageCracker = new MessageCracker(new LouieMessageHandler());
    }

    @Override
    public void onCreate(SessionID sessionID) {

    }

    @Override
    public void onLogon(SessionID sessionID) {

    }

    @Override
    public void onLogout(SessionID sessionID) {

    }

    @Override
    public void toAdmin(Message message, SessionID sessionID) {

    }

    @Override
    public void fromAdmin(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {

    }

    @Override
    public void toApp(Message message, SessionID sessionID) throws DoNotSend {

    }

    @Override
    public void fromApp(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        this.messageCracker.crack(message, sessionID);
    }
}
