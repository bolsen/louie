package org.louie.fix;

import quickfix.*;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.IOException;

/* Main entry point for running the FIX server */

public class LouieFixServer {

    /* The application object that implements the core routines for
     * delivering and sending order request/responses.
     */
    LouieFixApplication application;

    /* Stores settings about what sessions should look like. (Session being
    * and abstraction of communication between two counterparties.)
    *
    */
    private SessionSettings settings = null;


    /* The SocketAcceptor is what accepts requests. It is an implementation of AbstractSocketAcceptor
     * that uses a single thread to process requests (MINA is an evented network framework.)
     * The other option is using a ThreadedSocketAcceptor.
     *
     * For what it's worth, a new implementation can be utilized.
     */

    private Acceptor acceptor = null;

    /* Message store factory. In quickfix, the session creates a message store (file, memory, db)
     * when it starts running. The message store stores messages coming into the server to be processed
     * by the Application object.
     */
    MessageStoreFactory messageStoreFactory = null;

    /* Log messages to a source.
     */

    LogFactory logFactory = null;

    /* There are multiple versions of Fix and thus we need to select the right Message implementation.
     * The factory should do that job for us given the FIX version (passed by the party sending the message)
     */

    MessageFactory messageFactory = new DefaultMessageFactory();


    public LouieFixServer(LouieFixApplication louieFixApplication,
                          MessageStoreFactory messageStoreFactory,
                          SessionSettings settings,
                          LogFactory logFactory,
                          MessageFactory messageFactory) throws ConfigError {
        this.application         = louieFixApplication;
        this.messageStoreFactory = messageStoreFactory;
        this.settings            = settings;
        this.messageFactory      = messageFactory;
        this.logFactory          = logFactory;

        this.acceptor            = new SocketAcceptor(application, messageStoreFactory, settings, logFactory, messageFactory);
    }

    public void start() throws ConfigError {
        if(settings == null) {
            throw new RuntimeException("Cannot start server without settings defined.");
        }

        acceptor.start();

    }

    public void stop() {

    }

    /* Entry point to the Louie Server */

    public static void main(String args[]) {
        SessionSettings settings = null;

        try {
            settings = Configuration.getSessionSettingsConfiguration(args[0]);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        } catch (ConfigError e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }

        // Concrete application parts
        MessageStoreFactory messageStoreFactory = new MemoryStoreFactory();
        LogFactory logFactory = new ScreenLogFactory(settings);
        MessageFactory messageFactory = new DefaultMessageFactory();

        // Create a new server and start the server.
        LouieFixServer server = null;
        try {
            server = new LouieFixServer(
                    new LouieFixApplication(settings),
                    messageStoreFactory,
                    settings,
                    logFactory,
                    messageFactory
            );
        } catch (ConfigError configError) {
            configError.printStackTrace();
        }

        try {
            server.start();
        } catch (ConfigError configError) {
            System.err.println("Could not start server");
            configError.printStackTrace(System.err);
        }

        final LouieFixServer finalServer = server;
        Signal.handle(new Signal("TERM"), new SignalHandler() {

            @Override
            public void handle(Signal signal) {
                System.out.println("Stopping server.");
                finalServer.stop();
            }
        });


    }
}
