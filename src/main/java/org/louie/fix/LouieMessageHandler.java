package org.louie.fix;

/* Implements onMessage handlers for the Application. */

import quickfix.SessionID;
import quickfix.fix50.*;

public class LouieMessageHandler {

    public LouieMessageHandler() {

    }

    /*
     * When we get a new order, deliver it to the exchange and return a
     * ExecutionReport that the order has been accepted by the exchange.
     */

    public void onMessage(NewOrderSingle order, SessionID sessionID) {

    }

    /*
     * Try to cancel an order that is already in the exchange.
     */

    public void onMessage(OrderCancelRequest order, SessionID sessionID) {

    }

    /* We want to ask the exchange if the order was processed or not.
     * This delivers a confirmation on the status of that request.
     */

    public void onMessage(ConfirmationRequest confirm, SessionID sessionID) {

    }


    /*
     * Asking if we can get a market data stream. If the request is successful,
     * we can send a snapshot. (For now, only snapshots are allowed, so there has
     * to be multiple MarketDataRequests to get the snapshots (I don't see how to
     * specify endpoints for the client, unless the client is a dedicated market data
     * operator.)
     */
    public void onMessage(MarketDataRequest request, SessionID sessionID) {

    }
}
