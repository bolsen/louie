package org.louie.fix;

import quickfix.ConfigError;
import quickfix.SessionSettings;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/*
 * Static support methods for running the Fix server.
 */
public class Configuration {

    public static SessionSettings getSessionSettingsConfiguration(String fileName) throws IOException, ConfigError {
        InputStream inputStream;
        if (fileName == null) {
            inputStream = Configuration.class.getResourceAsStream("louie_fix.cfg");
        } else {
            inputStream = new FileInputStream(fileName);
        }

        SessionSettings settings = new SessionSettings(inputStream);

        inputStream.close();

        return settings;
    }

}
