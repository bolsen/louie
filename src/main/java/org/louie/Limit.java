package org.louie;
import java.util.LinkedList;

/**
 * A Limit is an entry in the OrderBook, either being a buy or sell side Limit.
 * It lists all the orders for a single given price.
 *
 * For example, For some given securityId, the limit represents one price, like 1.00,
 * and all the orders that are requested at 1.00.
 */

public class Limit {
    private final int securityId;
    LinkedList<Order> orders = new LinkedList<Order>();
    double limitPrice;
    private Order.OrderType orderType;

    private OrderBook orderBook;

    /**
     * Create a new limit with a price, a security id, and the order side (BUY or SELL)
     * @param limitPrice The price of the limit
     * @param securityId The id of the security the limit represents.
     * @param orderType BUY or SELL
     */

    public Limit(double limitPrice, int securityId, Order.OrderType orderType) {
        this.limitPrice = limitPrice;
        this.securityId = securityId;
        this.orderType  = orderType;
    }

    /**
     * How many orders are in this limit.
     * @return int The size of the limit.
     */

    public int size() {
        return orders.size();
    }

    /** Only add an order if the limit price on the Limit and the Order price match.
     *
     * @param order the order to add to the Limit
     * @see   Order
    */

    public void addOrder(Order order) {

        if (this.getLimitPrice() == order.getPrice() &&
            this.getOrderType() == order.getOrderType() &&
            this.getSecurityId() == order.getSecurityId()
                ) {
            this.orders.add(order);
            order.setLimit(this);
        }
    }

    /**
     * Remove the order from the limit.
     * @param order
     * @return Order The order that was removed.
     * @throws LimitException Removing the last order causes a order book removal. If the limit is not a part of an
     * order book, then this exception will be thrown.
     */

    public boolean removeOrder(Order order) throws LimitException {
        boolean result = this.orders.remove(order);
        if (size() == 0) {
            removeFromOrderBook();
        }

        return result;
    }

    /**
     * Removes itself from the order book.
     * @throws LimitException
     */

    public void removeFromOrderBook() throws LimitException {
        if(this.orderBook == null) {
            throw new LimitException(this, "Does not have order book");
        }

        this.orderBook.removeLimit(this);
    }

    /* Match and remove if match is found. Iterates through the order list to find the first item that matches
     * the order provided.
     *
     * @param order The order to match.
     *
     * @return Order
     *
     * @see match(Order, boolean)
     *
     * TODO: Allow multiple orders to match to meet the request. (CRITICAL)
     *
     */

    public Order match(Order order) throws LimitException {
        for (Order matchingOrder : this.orders) {
            if (matchingOrder.matches(order)) {
                this.removeOrder(matchingOrder);
                return matchingOrder;
            }
        }

        return null;
    }


    // Getters/setters

    public double getLimitPrice() {
        return limitPrice;
    }

    public Order.OrderType getOrderType() {
        return orderType;
    }

    public int getSecurityId() {
        return securityId;
    }

    public OrderBook getOrderBook() {
        return orderBook;
    }

    public void setOrderBook(OrderBook orderBook) {
        this.orderBook = orderBook;
    }
}
