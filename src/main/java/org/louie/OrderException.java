package org.louie;

public class OrderException extends Exception {
    private final Order order;

    public OrderException(Order order, String s) {
        super(s);
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}
