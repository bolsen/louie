package org.louie;

import java.util.*;

/*
 * The OrderBook contains, for some given security the current active buys and sell orders.
 *
 * The order book has 2 lists (here implemented as TreeMaps) and pointers to the interesting orders.
 *
 * There are 4 operations that can be performed:
 * - Order book snapshot: builds a list of the current orders on the buy and sell sides, presorted.
 * - add: Add an order to the book
 * - cancel: Cancel an order (deletion from the order book)
 * - execute: Upon a match, perform the trade, delete from order book.
 *
 * All actions are sent through an upstream handler, used for auditing purposes and to finish processing the request.
 *
 *
 * TODO: Some design considerations to approach:
 * 1. We need to test the order sorting out well
 * 2. Maybe store the prime orders in a separate variable
 * 3. Al orders should return receipts.
 * 4. We need to not use DOUBLE as a way to key our TreeMaps
 *
 */
public class OrderBook<T extends OrderBookUpstreamHandler> {

    private double lastPrice;
    private final int securityId;

    /*
     * Create a new order book that will deliver events to the upstream handler.
     *
     * @param OrderBookUpstreamHandler The object of a class implementing an upstream handler.
     */

    T handler;

    TreeMap<Integer, Limit> buyOrders = new TreeMap<Integer, Limit>(new OrderBookComparatorBuy());
    TreeMap<Integer, Limit> sellOrders = new TreeMap<Integer, Limit>(new OrderBookComparatorSell());

    // We are creating an index of orders here for quick cancellation.
    private HashMap<UUID, Order> uuidOrderHashMap = new HashMap<UUID, Order>();


    public OrderBook(int securityId) {
        this.securityId = securityId;
    }

    /*
     * Adds an order to the this order book.
     *
     */
    public boolean add(Order order) throws OrderBookException {
        if(!this.isRightBook(order)) {
            throw new OrderBookException(this, order, "Incorrect book for security.");
        }

        TreeMap<Integer, Limit> modOrders = this.getRightBookSide(order);
        Limit limit = modOrders.get(new Double(order.getPrice()).hashCode());
        if (limit == null) {
            limit = addNewLimit(order, modOrders);
        } else {
            limit.addOrder(order);
        }

        return true;
    }

    private Limit addNewLimit(Order order, TreeMap<Integer, Limit> modOrders) {
        Limit limit = new Limit(order.getPrice(), order.getSecurityId(), order.getOrderType());
        limit.setOrderBook(this);
        limit.addOrder(order);

        // Put the order into the appropriate tree map.
        modOrders.put(new Double(limit.getLimitPrice()).hashCode(), limit);

        // Store the same order into the uuid hash for quick cancels.
        this.uuidOrderHashMap.put(order.getUuid(), order);

        return limit;

    }

    /**
     * Gets a limit object.
     *
     * @param type the order type (BUY or SELL)
     * @param selectedPrice The price of the limit.
     * @return Limit
     */

    public Limit getLimit(Order.OrderType type, double selectedPrice) {
       TreeMap<Integer, Limit> side = this.getRightBookSide(type);
        return side.get(new Double(selectedPrice).hashCode());
    }

    private boolean isRightBook(Order order) {
        return this.getSecurityId() == order.getSecurityId(); 
    }

    /*
     * Cancel an order
     *
     * TODO: Should really return a receipt
     */

    public Order cancel(Order order) throws OrderException, LimitException, OrderBookException {
       return cancel(order.getUuid());
    }

    private Order cancel(UUID uuid) throws OrderException, LimitException, OrderBookException {
        Order order = popOrder(uuid);
        order.removeSelfFromLimit();
        this.uuidOrderHashMap.remove(order);
        return order;
    }

    private Order popOrder(UUID uuid) throws OrderBookException {
        Order order = this.uuidOrderHashMap.remove(uuid);
        if(order == null) { throw new OrderBookException(this, "Order was not found."); }
        return order;
    }

    /*
     * Executes an order. Tries to find a potential match as the very first action. Depending on what
     * was found, record the information in an ExecutionReport.
     *
     * @param order The Order to match.
     *
     * @returns ExecutionReport Details about the execution and what was found.
     *
     * TODO: There is no fast matching here? Fix.
     */

    public ExecutionReport execute(Order order) throws LimitException {
        ExecutionReport report = new ExecutionReport();

        TreeMap<Integer, Limit> book = this.getOppositeBookSide(order);
        Limit limit = book.get(new Double(order.getPrice()).hashCode());
        report.setLimit(limit);

        if (limit != null) {
            Order matchedOrder = limit.match(order);
            report.addMatchedOrder(matchedOrder);
        }

        return report;
    }


    /*
     * Return a snapshot of the current buys and sells.
     *
     * @returns ArrayList<Double>[] with buy side and sell side.
     */


    public ArrayList<Double>[] snapshot() {
        ArrayList[] snap = new ArrayList[2];
        ArrayList<Double> buys = new ArrayList<Double>();
        ArrayList<Double> sells = new ArrayList<Double>();
        for(Limit limit : buyOrders.values()) {
            buys.add(limit.getLimitPrice());
        }

        for(Limit limit : sellOrders.values()) {
            sells.add(limit.getLimitPrice());
        }

        snap[0] = buys;
        snap[1] = sells;

        return snap;
    }



    private TreeMap<Integer, Limit> getRightBookSide(Order order) {
        return getRightBookSide(order.getOrderType());
    }

    private TreeMap<Integer, Limit> getRightBookSide(Order.OrderType type) {
        if(type == Order.OrderType.BUY) {
            return buyOrders;
        } else {
            return sellOrders;
        }
    }


    private TreeMap<Integer, Limit> getOppositeBookSide(Order order) {
        if(order.getOrderType() == Order.OrderType.BUY) {
            return sellOrders;
        } else {
            return buyOrders;
        }

    }

    public HashMap<UUID, Order> getUuidOrderHashMap() {
        return uuidOrderHashMap;
    }

    public int getSecurityId() {
            return securityId;
        }

    public TreeMap<Integer, Limit> getBuyOrders() {
        return buyOrders;
    }

    public TreeMap<Integer, Limit> getSellOrders() {
        return sellOrders;
    }


    /**
     * Remote the limit from the order book.
     * @param limit
     */

    public void removeLimit(Limit limit) {
        getRightBookSide(limit.getOrderType()).remove(new Double(limit.getLimitPrice()).hashCode());
    }

    public double getLastPrice() {
        return lastPrice;
    }


    private class OrderBookComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            if(o1 < o2) {
                return -1;
            } else if (o1.equals(o2)) {
                return 0;
            } else {
                return 1;
            }

        }
    }

    private class OrderBookComparatorSell extends OrderBookComparator {
    }

    private class OrderBookComparatorBuy extends OrderBookComparator {
        @Override
        public int compare(Integer o1, Integer o2) {
            return super.compare(o1,o2);
        }
    }
}
