package org.louie;

import static org.louie.OrderQualifications.*;

/*
 * Generates a Receipt after processing a transaction.
 */
public class TransactionFactory {

    public static Transaction completeTransaction(Order order1, Order order2) {
        Order[] orderList = organizeBuyAndSellOrder(order1, order2);

        Transaction tx = new Transaction(orderList[0], orderList[1]);
        tx.trade();

        return tx;
    }
}
