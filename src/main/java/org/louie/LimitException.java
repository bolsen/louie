package org.louie;

public class LimitException extends Exception {
    private final Limit limit;

    public LimitException(Limit limit, String s) {
        super(s);
        this.limit = limit;
    }

    public Limit getLimit() {
        return limit;
    }
}
