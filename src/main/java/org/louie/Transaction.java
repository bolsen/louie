package org.louie;

/**
 * Receipt of an order that ran containing the two orders that matched and the fill information.
 * Specifies if the fill was PARTIAL or COMPLETE for the order book framework to make a decision whether to
 * keep the order around or not.
 */

public class Transaction {
    private final Order buy;
    private final Order sell;
    private FillType fillType;
    private double sellerTotalPrice = 0.00;
    private int buyerTotalUnits = 0;
    private int sellerUnitsLeft;


    public Transaction(Order buy, Order sell) {
        this.buy = buy;
        this.sell = sell;
        this.fillType = null;
    }


    public Order getSell() {
        return sell;
    }

    public Order getBuy() {
        return buy;
    }

    public FillType getFillType() {
        return fillType;
    }

    /*
     * Assuming that the orders are tradeable, store the orders and the
     * exchange properties.
     *
     * TODO: There is a few problems where we make incomplete assumptions
     * 1. We assume that we are matching prices
     */

    public void trade() {
        this.sellerTotalPrice = getBuy().getPrice() * getBuy().getUnits();
        this.buyerTotalUnits  = getBuy().getUnits();
        this.sellerUnitsLeft  = getSell().getUnits() - getBuy().getUnits();
        if (getSell().getUnits() - this.buyerTotalUnits > 0) {
            this.setFillType(FillType.PARTIAL);
        } else {
            this.setFillType(FillType.COMPLETE);
        }
    }

    /*
     * If the sell order is FillType.PARTIAL, then we can generate a new sell order
     * so it can be inserted back into the order book.
     */

    public Order generateNewSellOrder() {
        if(this.sell != null) {
            Order order = new Order(
                    getSell().getOrderType(), false,
                    getSell().getUnits() - this.buyerTotalUnits, getSell().getPrice(),
                    getSell().getSecurityId(), getSell().getUserId());

            return order;
        } else {
            return null;
        }
    }

    public void setFillType(FillType fillType) {
        this.fillType = fillType;
    }

    public int getBuyerTotalUnits() {
        return buyerTotalUnits;
    }

    public double getSellerTotalPrice() {
        return sellerTotalPrice;
    }

    public int getSellerUnitsLeft() {
        return sellerUnitsLeft;
    }

    public enum FillType {
        PARTIAL, COMPLETE
    }
}
