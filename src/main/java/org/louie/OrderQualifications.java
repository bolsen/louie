package org.louie;

/*
 * Static methods that handle different types of order qualifications.
 */
public class OrderQualifications {

    public static boolean areOrderTypesDifferent(Order order1, Order order2) {
        return order1.getOrderType() != order2.getOrderType();
    }

    public static boolean canPurchaseOccur(Order order1, Order order2) {
        Order[] orderList = organizeBuyAndSellOrder(order1, order2);
        Order buyOrder = orderList[0];
        Order sellOrder = orderList[1];

        return sellOrder.getPrice() <= buyOrder.getPrice();

    }

    public static boolean areSecurityIdsSame(Order order1, Order order2) {
        return order1.getSecurityId() == order2.getSecurityId();
    }

    public static boolean areUserIdsDifferent(Order order1, Order order2) {
        return order1.getUserId() != order2.getUserId();
    }

    /*
     * enoughUnits() needs to handle these scenarios:
     *
     * 1. If the SELL order is allOrNothing, then the BUY needs to match the unit length.
     * 2. If the SELL order is not allOrNothing, then the unit size for SELL must be greater than or equal to the BUY
     */


    public static boolean enoughUnits(Order order1, Order order2) {

        Order[] orderList = OrderQualifications.organizeBuyAndSellOrder(order1, order2);
        Order buyOrder = orderList[0];
        Order sellOrder = orderList[1];

        if (sellOrder.isAllOrNothing()) {
            return buyOrder.getUnits() == sellOrder.getUnits();
        } else {
            return sellOrder.getUnits() >= buyOrder.getUnits();
        }
    }


    /*
     * Convenience function to organize the buy and sell orders, for cases when we have to do logic
     * on the order type of an order.
     */

    public static Order[] organizeBuyAndSellOrder(Order order1, Order order2) {
        Order sellOrder = null;
        Order buyOrder  = null;
        if (order1.getOrderType() == Order.OrderType.SELL) {
            sellOrder = order1;
            buyOrder = order2;
        } else if (order2.getOrderType() == Order.OrderType.SELL) {
            sellOrder = order2;
            buyOrder = order1;
        }

        Order[] orderList = {buyOrder, sellOrder};

        return orderList;
    }

}
